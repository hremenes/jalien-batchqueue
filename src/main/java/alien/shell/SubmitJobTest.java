package alien.shell;
import java.io.IOException;

import alien.shell.BusyBox;

public class SubmitJobTest {
	public static void main(final String[] args) {
		BusyBox boombox; 
		try {
			boombox = new BusyBox("10.0.0.245", 8389, "Ytr43WWq99", "haakon", true);
			boombox.executeCommand("submit echo.jdl");
		} catch (IOException e) {
			System.out.println("BusyBox wouldnt start or work: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("More general exception handler " + e.getMessage());
		}
	}

}

