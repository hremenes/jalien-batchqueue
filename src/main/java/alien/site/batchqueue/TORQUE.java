package alien.site.batchqueue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.logging.Logger;

public class TORQUE extends BatchQueue {

	//private TreeSet<String> envFromConfig;
	private String submitCmd;
	private String submitArg = "";
	private String statusArg = "";
	private String statusCmd;
	private String killCmd;
	private TreeSet<String> envFromConfig;

	/**
	 * @param conf
	 * @param logr
	 */
	@SuppressWarnings("unchecked")
	public TORQUE(HashMap<String, Object> conf, Logger logr) {
		this.config = conf;
		this.logger = logr;
		this.envFromConfig = (TreeSet<String>) this.config.get("ce_environment");
		this.logger.info("This VO-Box is " + config.get("ALIEN_CM_AS_LDAP_PROXY") + ", site is "
				+ config.get("site_accountname"));
		
		this.submitCmd = (config.get("CE_SUBMITCMD") != null ? (String) config.get("CE_SUBMITCMD") : "qsub");
		this.statusCmd = (config.get("CE_STATUSCMD") != null ? (String) config.get("CE_STATUSCMD") : "qstat -n -1");
		this.killCmd = (config.get("CE_KILLCMD") != null ? (String) config.get("CE_KILLCMD") : "qdel");

		this.submitArg = getArgs("CE_SUBMITARG");
		this.statusArg = getArgs("CE_STATUSARG");
		
		if (envFromConfig != null) {
			for (String env_field : envFromConfig) {
				if (env_field.contains("SUBMIT_ARGS")) {
					this.submitArg = getValue(env_field, "SUBMIT_ARGS", this.submitArg);
				}
				if (env_field.contains("STATUS_ARGS")) {
					this.statusArg = getValue(env_field, "STATUS_ARGS", this.statusArg);
				}
			}
		}
		
		this.submitCmd = submitCmd + " " + submitArg;

	}

	@Override
	public void submit(final String script) {
		this.logger.info("Submit TORQUE");

		String submit_cmd = "#PBS -o /dev/null";
		submit_cmd += "#PBS -e /dev/null";
		submit_cmd += "#PBS -V \n";
		if (stageIn) {
			submit_cmd += "PBS -W stagein=$HOME/@" + config.get("host") + ":$HOME \n";
		}
		submit_cmd += submitArg;
		submit_cmd += script;
		String temp_file_cmd = this.submitCmd + " <<< $'" + submit_cmd + "'";
		ArrayList<String> output = executeCommand(temp_file_cmd);
		for (String line : output) {
			String trimmed_line = line.trim();
			this.logger.info(trimmed_line);
		}

	}

	
	public int getStatus(String status) {
		int numberedStatus = 0;
		ArrayList<String> out = executeCommand(statusCmd + " " + statusArg);
		if (out == null) {
			return -1;
		}
		for (String output_line : out) {
			String[] line = output_line.trim().split("\\s+");
			for (String job_line : line) {
				if (job_line.equals("Q") || job_line.equals(status)) {
					numberedStatus++;
				}
			}
		}
		return numberedStatus;
	}
	
	@Override
	public int getNumberActive() {
		return getStatus("R");
	}

	@Override
	public int getNumberQueued() {
		return getStatus("Q");
	}

	@Override
	public int kill() {
		this.logger.info("Checking proxy renewal service");
		ArrayList<String> kill_cmd_output = null;
		kill_cmd_output = executeCommand(this.killCmd);

		if (kill_cmd_output != null) {
			this.logger.info("Kill cmd output:\n");
			for (String line : kill_cmd_output) {
				line = line.trim();
				this.logger.info(line);
			}

		}
		return 0;
	}


}
