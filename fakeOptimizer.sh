#!/bin/bash

echo "Ceci n'est pas un Optimizer"

DBHOST='10.0.0.224'
DBPORT='3307'
DBPASS='Ytr43WWq99'
DBUSER='admin'
DB='processes'

MYSQLCALL="mysql --host=$DBHOST --port=$DBPORT --password=$DBPASS --user=$DBUSER -D $DB -e"

while :
do
	echo \"Optimizing\"

	#Transition "Inserting" jobs to "Waiting"
	$MYSQLCALL "update QUEUE set statusId=5 where statusId=1;"

	#Add JOBAGENT table entry
	$MYSQLCALL "insert into JOBAGENT(entryId,priority,noce,fileBroker,revision,price,partition,disk,ttl,oldestQueueId,ce,userId,packages,site,counter)values('1','100','','0','0','1','%','50000000','80000','0',',ALICE::JALIEN::CEJALIEN,','6','%',',JALIEN,',1)"	

	#Register jobs transitioned to "Waiting" with the JOBAGENT entry
	$MYSQLCALL  "update QUEUE set agentId=1 where statusId=5;"

	#Cleanup killed jobs
	$MYSQLCALL "delete from QUEUE where statusId='-14';"
	
	sleep 30
done
